all: main

HEADERS=solution.h
CFLAGS=-g -Wall

main: main.o solution.o
	gcc $(CFLAGS) -lpthread $^ -o $@

%.o: %.c $(HEADERS)
	gcc $(CFLAGS) -c $< -o $@

clean: 
	rm *.o
	rm main