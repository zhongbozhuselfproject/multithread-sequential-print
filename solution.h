#ifndef _SOLUTION_H
#define _SOLUTION_H

#include <pthread.h>

typedef struct serialize_thread_struct{
    pthread_spinlock_t lock;
    pthread_t * thread_array;
    unsigned int thread_num;
    // left for students
    unsigned int counter;
}st_lock;

int solution_init(st_lock* st, unsigned int num);
void solution_free(st_lock* st);
void print_incremental_number(st_lock* st, int thread_id);

#endif
