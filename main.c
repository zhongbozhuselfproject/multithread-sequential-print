#include <stdio.h>
#include <pthread.h>
#include "solution.h"

st_lock slock;

void * thread_func(void* input){
    int thread_id = *((int*)input);
    // printf("thread %d created\n", thread_id);
    int cnt = 5;
    while (cnt>0){
        print_incremental_number(&slock, thread_id);
        cnt--;
    }
    return 0;
}

int main(){
    int i;
    int thread_ids[] = {0,1,2};
    solution_init(&slock, 3);
    for (i=0; i<3; i++){
        pthread_create(&(slock.thread_array[i]), NULL, thread_func, (void*)(&thread_ids[i]));
    }
    for (i=0; i<3; i++){
        pthread_join(slock.thread_array[i], NULL);
    }
    solution_free(&slock);
    return 0;
}
