#include <stdlib.h>
#include <stdio.h>
#include "solution.h"

int solution_init(st_lock* st, unsigned int num){
    int ret;
    ret = pthread_spin_init(&(st->lock), PTHREAD_PROCESS_PRIVATE);
    if (ret<0)
        return -1;
    st->thread_array = (pthread_t *)malloc(sizeof(pthread_t)*num);
    if (st->thread_array == NULL)
        return -1;
    st->thread_num = num;
    st->counter = 0;
    return 0;
}

void solution_free(st_lock* st){
    pthread_spin_destroy(&(st->lock));
    free(st->thread_array);
}

void print_incremental_number(st_lock* st, int thread_id){
    while (1){
        pthread_spin_lock(&(st->lock));
        if (st->counter % st->thread_num == thread_id){
            printf("thread %d print value %u\n", thread_id, st->counter);
            st->counter ++;
            pthread_spin_unlock(&(st->lock));
            break;
        }
        pthread_spin_unlock(&(st->lock));
    }
}
